package main.com.unit;

public class TestProgram {
	
	public Boolean palin(String word) {
		Boolean finalStr;
		String reverse = ""; // Objects of String class
	     
	      int length = word.length();
	     
	      for (int i = length - 1; i >= 0; i--)
	         reverse = reverse + word.charAt(i);
	         
	      if (word.equals(reverse)) {
	    	  finalStr = true;
	      }
	      else {
	    	  finalStr = false;
	      }
	         
	      return finalStr;
	}
	
	
	public int square(int x) {
		return x*x;
	}
	
	
	public int countA(String word) {
		int count = 0;
		for(int i=0; i<= word.length()-1; i++) {
			if(word.charAt(i) == 'a' || word.charAt(i) == 'A'){
				count++;
			}
		}
		return count;
	}
	

}
